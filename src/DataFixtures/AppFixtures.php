<?php

namespace App\DataFixtures;
use Faker\Factory;
use Faker\Generator;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{/**
 * @var Generator
 */

private Generator $faker;

public function __construct(){
    $this->faker = Factory::create('fr_FR');
}



    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        for($i = 0; $i < 50; $i++) {
            $user = new Users();
            $user->setNom($this->faker->lastName())
                 ->setPrenom($this->faker->firstName())
                 ->setMail($this->faker->unique()->email())
                 ->setPseudo($this->faker->unique()->userName())
                 ->setMdp($this->faker->password());


        $manager->persist($user);
        }
        
        $manager->flush();
    }
}
