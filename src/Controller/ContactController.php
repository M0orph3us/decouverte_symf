<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'contact', methods: ["GET", "POST"])]
    public function index(): Response
    {
        return $this->render('contact.html.twig', [
            'controller_name' => 'ContactController',
            'Prénom' => 'Gaël'
        ]);
    }

    public function merci(): Response 
    {
        return $this->render('contact/merci.html.twig');
    }

    public function echec(): Response 
    {
        return $this->render('contact/echec.html.twig');
    }
}