<?php

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use App\Controller\ContactController;

return function (RoutingConfigurator $routes) {
    $routes->add('merci', '/contact/merci')
           ->controller([ContactController::class, 'merci'])

           ->add('echec', '/contact/echec')
           ->controller([ContactController::class, 'echec'])
    ;
};
